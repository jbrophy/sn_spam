### Twitter Data Setup ###

Use the following steps to add Twitter data:

1. Place `comments.csv` file in this directory.
2. If there is a follower graph for this data, place the `network.tsv` file in this directory as well.